public class CookieBoxing {
    private String name;
    private String ability;
    private String gender;
    private String pet;
    private String petability;

    public CookieBoxing(String name, String ability, String gender, String pet, String petability) {
        this.name = name;
        this.ability = ability;
        this.gender = gender;
        this.pet = pet;
        this.petability = petability;

    }

    public void printCookieBoxing() {
        System.out.println("Cookie Run Game");
        System.out.println("Name : " + name);
        System.out.println("Ability : " + ability);
        System.out.println("Gender : " + gender);
        System.out.println("Pet : " + pet);
        System.out.println("Pet Ability : " + petability);

    }

    public String getName() {
        return name;
    }
    public String getAbility() {
        return ability;
    }
    public String getGender() {
        return gender;
    }
    public String getPet() {
        return pet;
    }
    public String getPetability() {
        return petability;
    }
}


